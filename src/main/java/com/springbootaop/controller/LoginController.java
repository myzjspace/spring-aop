package com.springbootaop.controller;
import com.springbootaop.common.Log;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@ResponseBody
public class LoginController {

    @GetMapping("/")
    public String index(){
        return "index";
    }

    @GetMapping("/user/{id}")
    @Log("登录")
    public User login(@PathVariable("id") String userId){
        System.out.println("用户userId:"+userId+"开始请求");
        User user = new User();
        user.setPassword("123");
        user.setUsername("aaa");
        return user;
    }

}
