package com.springbootaop.common;
import com.google.gson.Gson;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

@Aspect//表示是一个切面类
@Component//加入了IOC容器
public class LogAspect {

    private final static Logger logger  = LoggerFactory.getLogger(LogAspect.class);

    /** 以自定义 @WebLog 注解为切点 */
    @Pointcut("@annotation(com.springbootaop.common.Log)")
    public void pointcut() {}

    /**
     * 在切点之前织入
     * @param joinPoint
     * @throws Throwable
     */
    @Before("pointcut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 开始打印请求日志
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        String methodDescription = getAspectLogDescription(joinPoint);

        // 打印请求相关参数
        logger.info("================== Start =======================");
        // 打印请求 url
        logger.info("请求地址: {}", request.getRequestURL().toString());
        // 打印描述信息
        logger.info("日志描述: {}", methodDescription);
        // 打印 Http method
        logger.info("请求方法: {}", request.getMethod());
        // 打印调用 controller 的全路径以及执行方法
        logger.info("调用路径 : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        // 打印请求的 IP
        logger.info("IP地址: {}", request.getRemoteAddr());
        // 打印请求入参
        logger.info("请求参数: {}", new Gson().toJson(joinPoint.getArgs()));
    }

    /**
     * 环绕
     * @param proceedingJoinPoint
     * @return
     * @throws Throwable
     */
    @Around("pointcut()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = proceedingJoinPoint.proceed();
        // 打印出参
        logger.info("请求返回 : {}", new Gson().toJson(result));
        // 执行耗时
        logger.info("执行耗时 : {} ms", System.currentTimeMillis() - startTime);
        logger.info("===================== End =======================");
        return result;
    }


    /**
     * 获取切面注解的描述
     *
     * @param joinPoint 切点
     * @return 描述信息
     * @throws Exception
     */
    public String getAspectLogDescription(JoinPoint joinPoint)
            throws Exception {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Class targetClass = Class.forName(targetName);
        Method[] methods = targetClass.getMethods();
        StringBuilder description = new StringBuilder();
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazz = method.getParameterTypes();
                if (clazz.length == arguments.length) {
                    description.append(method.getAnnotation(Log.class).value());
                    break;
                }
            }
        }
        return description.toString();
    }
}
